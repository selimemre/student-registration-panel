package com.example.s_e_s.assingment2;

import android.app.Activity;
import android.graphics.DashPathEffect;
import android.os.Bundle;
import android.os.Message;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.EmptyStackException;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Random;

public class MainActivity extends Activity {

    private TabHost myTabHost;
    private Button btnAdd, btnDelete, btnUpdate, btnUpdate2,btnCancel,btnSearch;
    private EditText edittxtFaculty, edittxtDepartment, edittxtInstructor, editStudentName, editStudentLastName,editID;
    private TextView studentID;
    private ListView listView,listView2;
    private ArrayList<String> list;
    private ArrayList<String> list2;
    private ArrayList<String> departmant = new ArrayList<String>();
    private ArrayList<String> faculty = new ArrayList<String>();
    private ArrayList<String> advisor = new ArrayList<String>();
    private ArrayList<Student> studentList = new ArrayList<Student>();
    private ArrayAdapter<String> adapter,adapter2,adapterFac,adapterDep,adapterInc;
    private int choice = 0;
    private Spinner facultySpinner;
    private Spinner departmentSpinner;
    private Spinner advisorSpinner;
    private RadioGroup radioGroup1;
    private RadioButton radioMale, radioFemale;
    private static final Random random = new Random();
    public int k=0;
    public String tempFaculty="";
    public String tempDepartment="";
    public String tempAdv="";
    public String tempFM="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // 2nd Tab
        facultySpinner = (Spinner) findViewById(R.id.FacultySpinner);
        departmentSpinner = (Spinner) findViewById(R.id.DepartmentSpinner);
        advisorSpinner = (Spinner) findViewById(R.id.AdvisorSpinner);

        radioGroup1 = (RadioGroup) findViewById(R.id.radiogroup1);
        radioFemale = (RadioButton) findViewById(R.id.radioFemale);
        radioMale = (RadioButton) findViewById(R.id.radioMale);
        radioMale.setChecked(true);
        studentID = (TextView) findViewById(R.id.StudentID);
        editStudentName = (EditText) findViewById(R.id.editStudentnName);
        editStudentLastName = (EditText) findViewById(R.id.editStudentLast);
        editID = (EditText) findViewById(R.id.searchID);
        btnUpdate2 = (Button) findViewById(R.id.btnUpdate2);


        //1st tab
        listView = (ListView) findViewById(R.id.listView);
        list = new ArrayList<String>();
        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, list);
        listView.setAdapter(adapter);

        listView2=(ListView)findViewById(R.id.listforStudents);
        list2=new ArrayList<String>();
        adapter2 = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, list2);
        listView2.setAdapter(adapter2);





        edittxtFaculty = (EditText) findViewById(R.id.edittxtFaculty);
        edittxtDepartment = (EditText) findViewById(R.id.edittxtDepartment);
        edittxtInstructor = (EditText) findViewById(R.id.edittxtInstructor);

        btnAdd = (Button) findViewById(R.id.btnAdd);
        btnDelete = (Button) findViewById(R.id.btnDelete);
        btnDelete.setEnabled(false);
        btnUpdate = (Button) findViewById(R.id.btnUpdate);
        btnUpdate.setEnabled(false);


        myTabHost = (TabHost) findViewById(R.id.tabhost);
        myTabHost.setup();

        TabHost.TabSpec tabSpec;

        //the first tab
        tabSpec = myTabHost.newTabSpec("Administration");
        tabSpec.setContent(R.id.tab1);
        tabSpec.setIndicator("Administration", null);
        myTabHost.addTab(tabSpec);

        //the second tab
        tabSpec = myTabHost.newTabSpec("Registration");
        tabSpec.setContent(R.id.tab2);
        tabSpec.setIndicator("Registration", null);

        myTabHost.addTab(tabSpec);

        //the third tab
        tabSpec = myTabHost.newTabSpec("Students");
        tabSpec.setContent(R.id.tab3);
        tabSpec.setIndicator("Students", null);
        myTabHost.addTab(tabSpec);

        myTabHost.setCurrentTab(0);





        facultySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                tempFaculty = ((String)adapterView.getItemAtPosition(i)).toString();

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        departmentSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                tempDepartment = ((String)adapterView.getItemAtPosition(i)).toString();

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        advisorSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                tempAdv = ((String)adapterView.getItemAtPosition(i)).toString();

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        radioGroup1.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton rb = (RadioButton) group.findViewById(checkedId);
                String take = rb.getText().toString();
                if (take.equals("Male")) {
                    tempFM = "Male";
                }
                if (take.equals("Female")) {
                    tempFM = "Female";
                }
            }
        });



        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            String t1;
            @Override

            public void onItemClick(final AdapterView<?> adapterView, View view, final int i, long l) {
                btnDelete.setEnabled(true);
                btnUpdate.setEnabled(true);
                 t1= list.get(i);

                String[] parts = t1.split("-");
                String part1 = parts[0]; // 004
                String part2 = parts[1]; // 034556
                String part3 = parts[2]; // 034556

                edittxtFaculty.setText(part1);
                edittxtDepartment.setText(part2);
                edittxtInstructor.setText(part3);


                btnDelete.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        list.remove(i);
                        adapter.notifyDataSetChanged();
                        btnDelete.setEnabled(false);
                        btnUpdate.setEnabled(false);
                    }
                });

                btnUpdate.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        faculty.remove(edittxtFaculty.getText().toString());
                        departmant.remove(edittxtDepartment.getText().toString());
                        advisor.remove(edittxtInstructor.getText().toString());
                        list.remove(t1);

                        try {

                            if (!edittxtFaculty.getText().toString().equals("") &&
                                    !edittxtDepartment.getText().toString().equals("") &&
                                    !edittxtInstructor.getText().toString().equals("")) {

                                faculty.add(edittxtFaculty.getText().toString());
                                departmant.add(edittxtDepartment.getText().toString());
                                advisor.add(edittxtInstructor.getText().toString());
                                adapterFac.notifyDataSetChanged();
                                adapterDep.notifyDataSetChanged();
                                adapterInc.notifyDataSetChanged();
                                list.add(edittxtFaculty.getText().toString()+"-"+edittxtDepartment.getText().toString()+"-"+edittxtInstructor.getText().toString());
                                adapter.notifyDataSetChanged();


                                edittxtFaculty.setText("");
                                edittxtDepartment.setText("");
                                edittxtInstructor.setText("");
                                Toast.makeText(getApplication(), "GREAT", Toast.LENGTH_SHORT).show();

                                btnUpdate.setEnabled(false);
                                btnDelete.setEnabled(false);
                            } else
                                throw new EmptyStackException();
                        } catch (EmptyStackException e) {
                            Toast.makeText(getApplication(), "Please fill the all empty spaces", Toast.LENGTH_SHORT).show();
                            e.printStackTrace();
                        }
                    }
                });
            }
        });

    }

    public void addClicked(View v) {

        try {

            if (!edittxtFaculty.getText().toString().equals("") &&
                    !edittxtDepartment.getText().toString().equals("") &&
                    !edittxtInstructor.getText().toString().equals("")) {


                list.add(edittxtFaculty.getText().toString() + "-" +
                        edittxtDepartment.getText().toString() + "-" +
                        edittxtInstructor.getText().toString());


                adapter.notifyDataSetChanged();
                faculty.add(edittxtFaculty.getText().toString());
                departmant.add(edittxtDepartment.getText().toString());
                advisor.add(edittxtInstructor.getText().toString());

                edittxtFaculty.setText("");
                edittxtDepartment.setText("");
                edittxtInstructor.setText("");



                adapterFac= new ArrayAdapter(this, android.R.layout.simple_spinner_item, DeleteRepated(faculty));
                adapterFac.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                facultySpinner.setAdapter(adapterFac);


                adapterDep = new ArrayAdapter(this, android.R.layout.simple_spinner_item, DeleteRepated(departmant));
                adapterDep.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                departmentSpinner.setAdapter(adapterDep);

                adapterInc = new ArrayAdapter(this, android.R.layout.simple_spinner_item, DeleteRepated(advisor));
                adapterInc.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                advisorSpinner.setAdapter(adapterInc);
                adapter.notifyDataSetChanged();

            } else
                throw new EmptyStackException();
        } catch (EmptyStackException e) {
            Toast.makeText(this, "Please fill the all empty spaces", Toast.LENGTH_SHORT).show();
        }

    }


    public ArrayList DeleteRepated(ArrayList input) {
        ArrayList<String> temp = new ArrayList<String>();
        LinkedHashSet<String> lhs = new LinkedHashSet<String>();
        lhs.addAll(input);
        temp.addAll(lhs);
        return temp;
    }

    public void Register(View v) {
        try {
            if(!(editStudentLastName.getText().toString().equals("")) && !(editStudentName.getText().toString().equals(""))) {
                k = random.nextInt(999999999) + 1000000000;
                studentID.setText(String.valueOf(k));
                Student newStudent = new Student(editStudentName.getText().toString(), editStudentLastName.getText().toString(), tempAdv, tempFaculty, tempDepartment, k, tempFM);
                studentList.add(newStudent);
                String write = "";
                for (int k = 0; k < studentList.size(); k++) {
                    int g = Integer.parseInt(studentID.getText().toString());
                    if (studentList.get(k).studentID == g) {
                        write = studentList.get(k).returnALL();
                    }
                }
                list2.add(write);
                adapter2.notifyDataSetChanged();
                Toast.makeText(this, "REGISTER IS SUCCESFULL", Toast.LENGTH_SHORT).show();
            }
            else{
                throw new EmptyStackException();
            }
        }
        catch (EmptyStackException e){
            Toast.makeText(this,"StudentName and StudentLastName can not be empty",Toast.LENGTH_SHORT).show();
        }
    }

    public void Search (View v){

        try {
            if(!(studentID.getText().toString().equals(""))) {
                for (int i = 0; i < studentList.size(); i++) {
                    Student tempS = studentList.get(i);
                    int positionF = 0;
                    int positionD = 0;
                    int positionA = 0;
                    if (tempS.studentID == Integer.parseInt(editID.getText().toString())) {
                        editStudentName.setText(tempS.name);
                        editStudentLastName.setText(tempS.lastname);
                        for (int k = 0; k < faculty.size(); k++) {
                            String tempF = faculty.get(k);
                            if (tempS.faculty.equals(tempF)) {
                                positionF = k;
                            }
                        }
                        for (int l = 0; l < departmant.size(); l++) {
                            String tempD = departmant.get(l);
                            if (tempS.department.equals(tempD)) {
                                positionD = l;
                            }
                        }
                        for (int p = 0; p < advisor.size(); p++) {
                            String tempA = advisor.get(p);
                            if (tempS.advisor.equals(tempA)) {
                                positionF = p;
                            }
                        }
                        facultySpinner.setSelection(positionF);
                        departmentSpinner.setSelection(positionD);
                        advisorSpinner.setSelection(positionA);

                        if (tempS.fm.equals(radioFemale.getText().toString())) {
                            radioMale.setChecked(false);
                            radioFemale.setChecked(true);
                        }
                        else {
                            radioMale.setChecked(true);
                            radioFemale.setChecked(false);
                        }
                            btnUpdate2.setEnabled(true);
                    }
                }
            }
            else{
                throw new EmptyStackException();
            }
        }
        catch (EmptyStackException e){
            Toast.makeText(this,"StudentID can not be empty",Toast.LENGTH_SHORT).show();
        }
    }
    public void update(View v){
        String write = "";
        for (int k = 0; k < studentList.size(); k++) {
            int g = Integer.parseInt(editID.getText().toString());
            if(studentList.get(k).studentID == g){
                write=studentList.get(k).returnALL();
                for (int p = 0; p < list2.size(); p++) {
                    String tempA = list2.get(p);
                    if (tempA.equals(write)){
                        list2.remove(tempA);
                        studentList.get(k).setName(editStudentName.getText().toString());
                        studentList.get(k).setLastname(editStudentLastName.getText().toString());
                        studentList.get(k).setAdvisor(tempAdv);
                        studentList.get(k).setFaculty(tempFaculty);
                        studentList.get(k).setDepartment(tempDepartment);
                        write=studentList.get(k).returnALL();
                        list2.add(write);
                    }
                }
             }
        }
        adapter2.notifyDataSetChanged();
        Toast.makeText(this,"UPDATE IS SUCCESFULL",Toast.LENGTH_SHORT).show();
    }

    public void update1(View v){
        faculty.remove(edittxtFaculty);
    }

    public void cancel(View v){
        System.exit(0);
    }


}
