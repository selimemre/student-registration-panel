package com.example.s_e_s.assingment2;

/**
 * Created by s_e_s on 23.12.2017.
 */

public class Student {
    String name,lastname,advisor,faculty,department,fm;
    int studentID;
    public Student (String name ,String Lastname, String Adv,String fac , String dep ,int ID , String fm){
        this.name=name;
        this.lastname=Lastname;
        this.advisor=Adv;
        this.studentID=ID;
        this.faculty=fac;
        this.department=dep;
        this.fm = fm;
     }

    public void setAdvisor(String advisor) {
        this.advisor = advisor;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public void setFaculty(String faculty) {
        this.faculty = faculty;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setStudentID(int studentID) {
        this.studentID = studentID;
    }

    public int getStudentID() {
        return studentID;
    }

    public String getAdvisor() {
        return advisor;
    }

    public String getDepartment() {
        return department;
    }

    public String getFaculty() {
        return faculty;
    }

    public String getLastname() {
        return lastname;
    }

    public String getName() {
        return name;
    }

    public String returnALL(){
        return studentID+" - "+name+"  "+lastname+" - "+faculty+" - "+department+" - "+advisor+" - "+fm;
    }

}
